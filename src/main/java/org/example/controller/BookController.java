package org.example.controller;

import org.example.model.StructBook;
import org.example.service.BookServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// CRUD classique et système de recherche qui vérifie que la chaîne de caractère recherché est contenue dans le nom de
// l'auteur ou dans le titre du livre

@RestController
@RequestMapping("/book")
public class BookController {

    private final BookServiceImpl bookService;

    public BookController(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @PostMapping()
    public ResponseEntity<StructBook> createBook(@RequestBody StructBook book) {
        StructBook createdBook = bookService.createBook(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdBook);
    }

    @GetMapping("/all")
    public ResponseEntity<List<StructBook>> getAllBooks() {
        List<StructBook> books = bookService.getAllBooks();
        return ResponseEntity.ok().body(books);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StructBook> getBookById(@PathVariable long id) {
        StructBook book = bookService.getBookById(id);
        return ResponseEntity.ok().body(book);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructBook> updateBook(@PathVariable long id, @RequestBody StructBook book) {
        StructBook updatedBook = bookService.updateBook(id, book);
        return ResponseEntity.ok(updatedBook);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructBook> deleteBook(@PathVariable long id) {
        StructBook deletedBook = bookService.deleteBook(id);
        return ResponseEntity.ok().body(deletedBook);
    }

    @GetMapping("/search/{search}")
    public ResponseEntity<List<StructBook>> searchBooks(@PathVariable String search) {
        List<StructBook> matchingBooks = bookService.searchBooks(search);
        return ResponseEntity.ok().body(matchingBooks);
    }
}