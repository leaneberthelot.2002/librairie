package org.example.controller;
import org.example.model.StructBorrow;
import org.example.service.BorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;


// Les emprunts ne sont pas réellement supprimés de la bd une fois les livres rendus, il change de statut afin de garder
// un historique des emprunts. Il existe tout de même une fonctionnalité de suppression dans le cas d'une erreur.

//J'ai fixé la duré d'emprunt d'un livre a une semaine, mais la date de rendu attendu du livre peut être modifié.

// Un livre ne peut être emprunter que si on a pas dépassé le nombre max d'emprunt en cours et que le livre est disponible,
// c'est-à-dire que le nombre d'emprunts dont non rendu est inférieur au nombre max de livre disponible

//L'id de l'utilisateur est dans les routes, mais il devrait être passé par le token.

@RestController
@RequestMapping("/borrow")
public class BorrowController {

    private final BorrowService borrowService;

    @Autowired
    public BorrowController(BorrowService borrowService) {
        this.borrowService = borrowService;
    }

    // TODO Implement token authentication with automatic retrieval of user ID
    @PostMapping("/{userId}/{bookId}")
    public ResponseEntity<StructBorrow> borrowBook(@PathVariable long userId, @PathVariable long bookId) {
        StructBorrow borrow = borrowService.borrowBook(userId, bookId);
        return ResponseEntity.status(HttpStatus.CREATED).body(borrow);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<StructBorrow>> getAllBorrowsByUserId(@PathVariable long userId) {
        List<StructBorrow> borrows = borrowService.getAllBorrowsByUserId(userId);
        return ResponseEntity.ok(borrows);
    }

    @PutMapping("/{borrowId}")
    public ResponseEntity<StructBorrow> returnBook(@PathVariable long borrowId, @RequestBody StructBorrow borrow) {
        StructBorrow newBorrow = borrowService.updateBorrow(borrowId, borrow);
        return ResponseEntity.ok(newBorrow);
    }

    @DeleteMapping("/Completed/{borrowId}")
    public ResponseEntity<StructBorrow> returnBook(@PathVariable long borrowId) {
        StructBorrow newBorrow = borrowService.returnBook(borrowId);
        return ResponseEntity.ok(newBorrow);
    }
    @DeleteMapping("/{borrowId}")
    public ResponseEntity<StructBorrow> deleteBorrow(@PathVariable long borrowId) {
        StructBorrow deletedBorrow = borrowService.returnBook(borrowId);
        return ResponseEntity.ok(deletedBorrow);
    }
}

