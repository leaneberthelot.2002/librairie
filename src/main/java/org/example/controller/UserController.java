package org.example.controller;

import org.example.model.StructUser;
import org.example.service.UserServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


// Pas de gestion des token ni de vrai login ou register, mais mdp crypté

// Le nombre max de livre est indépendant pour chaque utilisateur

@RequestMapping("/user")
public class UserController {
    private final UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping()
    public ResponseEntity<StructUser> createUser(@RequestBody StructUser user) {
        StructUser createdUser = userService.createUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @GetMapping("/all")
    public ResponseEntity<List<StructUser>> getAllUsers() {
        List<StructUser> Users = userService.getAllUsers();
        return ResponseEntity.ok().body(Users);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StructUser> getUserById(@PathVariable long id) {
        StructUser User = userService.getUserById(id);
        return ResponseEntity.ok().body(User);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructUser> updateUser(@PathVariable long id, @RequestBody StructUser User) {
        StructUser updatedUser = userService.updateUser(id, User);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructUser> deleteUser(@PathVariable long id) {
        StructUser deletedUser = userService.deleteUser(id);
        return ResponseEntity.ok().body(deletedUser);
    }
}
