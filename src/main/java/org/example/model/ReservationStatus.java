package org.example.model;

public enum ReservationStatus {
    PENDING("Pending"),
    COMPLETED("Completed");

    private final String status;

    ReservationStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}

