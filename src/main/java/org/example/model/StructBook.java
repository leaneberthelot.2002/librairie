package org.example.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Table(name = "book")
@Entity
public class StructBook {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String author;

    private Date publishDate;

    private int nbAvailableBooks;

}
