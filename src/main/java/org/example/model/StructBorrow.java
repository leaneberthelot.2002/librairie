package org.example.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Table(name = "boorow")
@Entity
public class StructBorrow {

    @Id
    @GeneratedValue
    private Long id;

    private Long bookId;

    private Long userId;

    private Date startDate;

    private Date endDate;

    private ReservationStatus status;
}
