package org.example.repository;

import org.example.model.StructBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StructBookRepository extends JpaRepository<StructBook, Long> {

    List<StructBook> findByAuthorContainingIgnoreCaseOrTitleContainingIgnoreCase(String author, String title);
}
