package org.example.repository;

import org.example.model.ReservationStatus;
import org.example.model.StructBorrow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StructBorrowRepository extends JpaRepository<StructBorrow, Long> {

    List<StructBorrow> findByUserId(long userId);

    List<StructBorrow> findByUserIdAndStatus(Long userId, ReservationStatus status);

    List<StructBorrow> findByBookIdAndStatus(Long bookId, ReservationStatus status);

}
