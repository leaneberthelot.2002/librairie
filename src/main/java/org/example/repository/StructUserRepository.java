package org.example.repository;

import org.example.model.StructUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StructUserRepository extends JpaRepository<StructUser, Long> {

    StructUser findByEmail(String email);
}
