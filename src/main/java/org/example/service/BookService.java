package org.example.service;

import org.example.model.StructBook;

import java.util.List;

public interface BookService {
    public StructBook createBook(StructBook book);

    public List<StructBook> getAllBooks();

    public StructBook getBookById(long id);

    public StructBook updateBook(long id, StructBook book);

    public StructBook deleteBook(long id);

    public List<StructBook> searchBooks(String search);
}
