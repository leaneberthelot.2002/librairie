package org.example.service;


import org.example.exeption.ResourceNotFoundException;
import org.example.model.StructBook;
import org.example.repository.StructBookRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private final StructBookRepository repo;

    public BookServiceImpl(StructBookRepository repo) {
        this.repo = repo;
    }

    @Override
    public StructBook createBook(StructBook book) {
        try {
            repo.save(book);
            return book;
        } catch (Exception e) {
            throw new RuntimeException("Error creating book: " + e.getMessage(), e);
        }
    }

    @Override
    public List<StructBook> getAllBooks() {
        return repo.findAll();
    }

    @Override
    public StructBook getBookById(long id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book not exist with id: " + id));
    }

    @Override
    public StructBook updateBook(long id, StructBook book) {
        try {
            StructBook updateBook = repo.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Book not exist with id: " + id));

            Optional.ofNullable(book.getTitle())
                    .orElseThrow(() -> new IllegalArgumentException("Title must not be null"));

            updateBook.setNbAvailableBooks(book.getNbAvailableBooks());
            updateBook.setTitle(book.getTitle());
            updateBook.setAuthor(book.getAuthor());
            updateBook.setPublishDate(book.getPublishDate());

            repo.save(updateBook);
            return updateBook;
        } catch (Exception e) {
            throw new RuntimeException("Error updating book: " + e.getMessage(), e);
        }
    }

    @Override
    public StructBook deleteBook(long id) {
        try {
            StructBook deletedBook = repo.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Book not exist with id: " + id));

            repo.deleteById(id);
            return deletedBook;
        } catch (Exception e) {
            throw new RuntimeException("Error deleting book: " + e.getMessage(), e);
        }
    }

    @Override
    public List<StructBook> searchBooks(String search) {
        return repo.findByAuthorContainingIgnoreCaseOrTitleContainingIgnoreCase(search, search);
    }
}
