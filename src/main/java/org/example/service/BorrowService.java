package org.example.service;

import org.example.model.StructBook;
import org.example.model.StructBorrow;
import org.example.model.StructUser;
import org.springframework.stereotype.Service;

import java.util.List;

public interface BorrowService {

    public StructBorrow borrowBook(long userId, long bookId);

    public List<StructBorrow> getAllBorrowsByUserId(long userId);

    public StructBorrow returnBook(long borrowId);

    public List<StructBorrow> getAllBorrows();

    public StructBorrow getBorrowById(long borrowId);

    public StructBorrow updateBorrow(long borrowId, StructBorrow borrow);

    public StructBorrow deleteBorrow(long borrowId);

    public StructBorrow createBorrow(Long userId, Long bookId);
}