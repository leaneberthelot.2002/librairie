package org.example.service;

import org.example.exeption.ResourceNotFoundException;
import org.example.model.ReservationStatus;
import org.example.model.StructBook;
import org.example.model.StructBorrow;
import org.example.model.StructUser;
import org.example.repository.StructBookRepository;
import org.example.repository.StructBorrowRepository;
import org.example.repository.StructUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BorrowServiceImpl implements BorrowService {

    private final StructBorrowRepository borrowRepository;
    private final StructBookRepository bookRepository;
    private final StructUserRepository userRepository;

    @Autowired
    public BorrowServiceImpl(StructBorrowRepository borrowRepository, StructBookRepository bookRepository, StructUserRepository userRepository) {
        this.borrowRepository = borrowRepository;
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }

    @Override
    public StructBorrow borrowBook(long userId, long bookId) {
        StructUser user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + userId));

        StructBook book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book not found with id: " + bookId));

        // Check if the user has reached the maximum number of borrows
        if (borrowRepository.findByUserIdAndStatus(userId, ReservationStatus.PENDING).size() >= user.getMaxBorrows()) {
            throw new RuntimeException("User has reached the maximum number of borrows.");
        }

        // Check if the book is available for borrowing (status is PENDING)
        if (borrowRepository.findByBookIdAndStatus(bookId, ReservationStatus.PENDING).size() >= book.getNbAvailableBooks()) {
            throw new RuntimeException("book is not available");
        }

        StructBorrow borrow = createBorrow(userId, bookId);

        return borrow;
    }


    @Override
    public List<StructBorrow> getAllBorrowsByUserId(long userId) {
        return borrowRepository.findByUserId(userId);
    }

    //set the borrow status to completed
    @Override
    public StructBorrow returnBook(long borrowId) {
        StructBorrow borrow = borrowRepository.findById(borrowId)
                .orElseThrow(() -> new ResourceNotFoundException("Borrow not found with id: " + borrowId));

        // Check if the book can be returned
        if (borrow.getStatus() == ReservationStatus.PENDING) {
            borrow.setStatus(ReservationStatus.COMPLETED);
            borrowRepository.save(borrow);
        } else {
            throw new RuntimeException("Cannot return the book. Borrow status is not pending.");
        }

        return borrow;
    }

    @Override
    public List<StructBorrow> getAllBorrows() {
        return borrowRepository.findAll();
    }

    @Override
    public StructBorrow getBorrowById(long borrowId) {
        return borrowRepository.findById(borrowId)
                .orElseThrow(() -> new ResourceNotFoundException("Borrow not found with id: " + borrowId));
    }


    // set the end date
    @Override
    public StructBorrow updateBorrow(long borrowId, StructBorrow borrow) {
        StructBorrow updateBorrow = borrowRepository.findById(borrowId)
                .orElseThrow(() -> new ResourceNotFoundException("Borrow not found with id: " + borrowId));

        updateBorrow.setEndDate(borrow.getEndDate());

        borrowRepository.save(updateBorrow);
        return updateBorrow;
    }


    // delete a borrow (not an update of the status)
    @Override
    public StructBorrow deleteBorrow(long borrowId) {
        StructBorrow deletedBorrow = borrowRepository.findById(borrowId)
                .orElseThrow(() -> new ResourceNotFoundException("Borrow not found with id: " + borrowId));

        borrowRepository.deleteById(borrowId);
        return deletedBorrow;
    }

    @Override
    public StructBorrow createBorrow(Long userId, Long bookId) {
        StructBorrow borrow = new StructBorrow();
        borrow.setUserId(userId);
        borrow.setBookId(bookId);
        borrow.setStartDate(new Date());

        // Set end date logic: 1 week after the start date
        long oneWeekInMillis = 7 * 24 * 60 * 60 * 1000L;
        borrow.setEndDate(new Date(borrow.getStartDate().getTime() + oneWeekInMillis));

        borrow.setStatus(ReservationStatus.PENDING);

        borrowRepository.save(borrow);

        return borrow;
    }
}
