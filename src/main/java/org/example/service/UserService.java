package org.example.service;

import org.example.model.StructBook;
import org.example.model.StructUser;

import java.util.List;

public interface UserService {
    public StructUser createUser(StructUser user);

    public StructUser getUserById(long id);

    public List<StructUser> getAllUsers();

    public StructUser updateUser(long id, StructUser user);

    public StructUser deleteUser(long id);
}