package org.example.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.example.exeption.ResourceNotFoundException;
import org.example.exeption.ValidationError;
import org.example.exeption.ValidationException;
import org.example.model.StructUser;
import org.example.repository.StructUserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private StructUserRepository repo;
    private BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(StructUserRepository repo, BCryptPasswordEncoder passwordEncoder) {
        this.repo = repo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public StructUser createUser(StructUser user) {
        List<ValidationError> validationErrors = new ArrayList<>();

        validateEmail(user.getEmail(), validationErrors);

        validatePassword(user.getPassword(), validationErrors);

        if (!validationErrors.isEmpty()) {
            throw new ValidationException("Validation failed for user", validationErrors);
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        repo.save(user);
        return user;
    }

    private void validateEmail(String email, List<ValidationError> errors) {
        if (!EmailValidator.getInstance().isValid(email)) {
            errors.add(new ValidationError("email", "Invalid email address."));
        }
    }

    private void validatePassword(String password, List<ValidationError> errors) {
        if (password.length() < 8) {
            errors.add(new ValidationError("password", "Password must be at least 8 characters long."));
        }
    }

    @Override
    public StructUser getUserById(long id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with id: " + id));
    }

    @Override
    public List<StructUser> getAllUsers() {
        return repo.findAll();
    }

    @Override
    public StructUser updateUser(long id, StructUser user) {
        StructUser updateUser = repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with id: " + id));

        updateUser.setEmail(user.getEmail());
        updateUser.setName(user.getName());
        updateUser.setPassword(passwordEncoder.encode(user.getPassword()));
        updateUser.setMaxBorrows(user.getMaxBorrows());

        repo.save(updateUser);
        return updateUser;
    }

    @Override
    public StructUser deleteUser(long id) {
        StructUser deletedUser = repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with id: " + id));

        repo.deleteById(id);
        return deletedUser;
    }
}
